from sklearn import metrics
import birm
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt


def build_righ_answers_array(relevant_sentences, sentences):
    righ_answers_array = []
    for s in sentences:
        if s in relevant_sentences:
            righ_answers_array.append(1)
        else:
            righ_answers_array.append(0)
    return righ_answers_array

def build_method_answers_array(w, sentences_map):
    righ_answers_array = []
    keys = sentences_map.keys()
    for s in keys:
        if sentences_map.get(s) > w:
            righ_answers_array.append(1)
        else:
            righ_answers_array.append(0)
    return righ_answers_array

#???????
def convert_to_percents(BIRM_sentences_weights_map):
    max = 0.1
    result_array = []
    for i in BIRM_sentences_weights_map.keys():
        result_array.append(BIRM_sentences_weights_map.get(i))
        if BIRM_sentences_weights_map.get(i)>max:
            max=i
    for j in result_array:
        j=j/max




birm.main()

relevant_sentences = []
relevant_sentences.append("Именно поэтому не стоит всем зацикливаться только лишь на сидячей работе".decode("utf-8"))
relevant_sentences.append(" Свои версии  существуют и у французов и у бельгийцев".decode("utf-8"))
relevant_sentences.append("Как известно с возрастом у человека уменьшается подвижность суставов".decode("utf-8"))

BIRM_sentences_weights_map = BIRM.main()
righ_answers_array = build_righ_answers_array(relevant_sentences, BIRM_sentences_weights_map.keys())

print (righ_answers_array)

w = 10.0 #Пороговое значение для определения релевантен документ или нет

BIRM_answers_array = build_method_answers_array(w, BIRM_sentences_weights_map)

print (BIRM_answers_array)

print(metrics.precision_score(righ_answers_array, BIRM_answers_array))

print (metrics.recall_score(righ_answers_array, BIRM_answers_array))

print (metrics.f1_score(righ_answers_array, BIRM_answers_array))


# BIRM_predictions =
# истинные значения классов, “золотой стандарт”
actual = [1,1,1,0,0,0]
# полученные вероятности принадлежности к классу
predictions = [18,0,9,0,0,18]
false_positive_rate, true_positive_rate, thresholds = roc_curve(righ_answers_array, BIRM_answers_array)
print (thresholds)
roc_auc = auc(false_positive_rate, true_positive_rate)
plt.title('Receiver Operating Characteristic')
plt.plot(false_positive_rate, true_positive_rate, 'b',
label='AUC = %0.2f'% roc_auc)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1],'r--')
plt.xlim([-0.1,1.2])
plt.ylim([-0.1,1.2])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()