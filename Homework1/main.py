import os
import re
import codecs
from lxml import etree

def fileToXml(directory):
    files = os.listdir(directory)
    page = etree.Element('Articles')

    for file in files:
        headElt = etree.SubElement(page, 'Article')
        f = codecs.open(os.path.join(directory, file), "r", "utf-8").read()
        sentences = [x for x in re.split(r"[\.!?\n]", f) if re.match(r"[\s]", x)]
        for sentence in sentences:
            title = etree.SubElement(headElt, 'sent')
            title.text = re.sub(r"[$«»–()\.,%/—\-:;\n]", '', sentence)
            with codecs.open(directory + '.xml', 'w', "utf-8") as output:
                output.write(etree.tounicode(page , pretty_print=True))

directory1 = 'Sport'
directory2 = 'Politics'
fileToXml(directory1)
fileToXml(directory2)